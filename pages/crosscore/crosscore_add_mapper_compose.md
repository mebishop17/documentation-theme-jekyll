---
title: Add Mapper to Docker Compose File
series: crosscore
weight: 1
sidebar: crosscore_sidebar
permalink: crosscore_add_mapper_compose.html
folder: crosscore
---

{% include custom/series_crosscore.html %}


The following process explains how to configure the files above and execute the mapper configuration.

1. To configure the `<MapperName>` mapper, edit the `docker-compose.yml` file with a text editor (for example vi) and add the following `<MapperName>` mapper section (the values used are examples).

    ```yaml
    mapper-<MapperName>:
      container_name: 'mapper-<MapperName>'
      image: 'redbox-docker-release-local.eda-prod-hud03.uk.experian.local:2222/mapper-<MapperName>:<version>'
      environment:
        DW_ZOOKEEPER_CONNECT_STRING: 'zookeeper.experian.local:2181' # change to host:port of zookeeper
        DW_ZOOKEEPER_KEY_STORE_PASSWORD: 'secret123' # change to password of keystore-zookeeper.jks
        DW_DISCOVERY_HOST: 'mapper.experian.local' # change to hostname of server running this container
        DW_DISCOVERY_PORT: '8443'
      volumes:
        - '/data/config/map/keystore-zookeeper.jks:/opt/crosscore/keys/keystore-zookeeper.jks'
      ports:
        - '<PORT>:8443'
    ```


1. Each mapper within Docker listens on port `8443` by default, but if several mappers are running on the Docker host, they can't all be exposed on the same external port. You must expose a port on the mapper server for each mapper deployed. Configure each mapper to use different ports and set the `DW_DISCOVERY_HOST` value and variable to match that port.

1. Save changes to the file.

1. Run the following command to install the `<MapperName>` mapper:

    ```bash
    sudo docker-compose -f <path to config file>/docker-compose.yml up -d
    ```
    
1. Replace `docker-compose.yml` with the actual name of your YAML file.


{% include custom/series_crosscore_next.html %}

