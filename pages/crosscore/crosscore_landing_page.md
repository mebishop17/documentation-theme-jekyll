---
title: Mapper Configuration Guide
toc: false
series: crosscore
weight: 0
permalink: crosscore_landing_page.html
sidebar: crosscore_sidebar
folder: crosscore
---

{% include custom/series_crosscore.html %}

This series of tasks walks you through how to configure Mapper. To use this template, you must modify the `<MapperName>` placeholder. In the code references, replace `<MapperName>` with the mapper name in lower case (for example, `newmapper`). In all other references, capitalize the mapper name as the proper noun (for example, `New Mapper`).

## Guide Overview

You must also review the following sections and update as needed so that it is specific to your mapper:

<br/>


1. [Add Mapper to Docker Compose File](./crosscore_add_mapper_compose.html)

    Add the `<MapperName>` Mapper to the `docker-compose.yml` file. This will be provided by the CC Mapper team when the RC build is complete.

1.  [Copy the Mapper Resources](./crosscore_copy_mapper.html)

    Copy the `<MapperName>` Mapper resources folder from Artifactory to the environment. This is provided by the team who builds the mapper. You need to update the list of artifacts that are included in the resources.zip archive file.

1.  **Update the Parameters in the `mapper-config.json` File**

    This is provided by the team who builds the mapper. You need to update the code block to include the correct configuration parameters and default/placeholder values and the table to include the correct description of those configuration parameters.

<br/>


Those are all the sections that need to be updated at this time. The others sections are static and can be used as is as long as no changes are made to the configuration process. That being said, your team needs to work closely with the IT/Ops team who deploy and configure your mappers to ensure that they have the information that they need to successfully deploy and configure your mapper.

## Assumptions

- You've set up the CrossCore environment.
- You've uploaded the mapper. 

This document will specifically outline the `<MapperName>` mapper and the configuration required for setting up a working call through the CrossCore environment.

## Configuration to be Performed by the IT Team

The following steps are required to modify a CrossCore test environment to include the `<MapperName>` mapper.

<br/>

{% include note.html content="If configuring the mappers for UAT, remember that `<MapperName>` UAT is a customer-facing environment." %}

<br/>

`<MapperName>` mapper configuration requires updates to the following files:

| File                 | Description                                                    |
| -------------------- | -------------------------------------------------------------- |
| `docker-compose.yml` | The Docker Compose file.                                       |
| `mapper-config.json` | The JSON file containing mapper configuration key-value pairs. |


You may see some scripts included in the mapper configuration artifact package. The scripts are used for internal testing. You must have root access to run the scripts successfully. Because most delivery and IT teams do not have root access permissions, the instructions on this page describe how to configure the mapper using the mapper-config-cli tool. These are the recommended instructions from the CrossCore team.

<br/>

The configuration of these files can be broken down into these steps:

<br/>

1. Add the `<MapperName>` mapper to the `docker-compose.yml` file.

1. Copy the `<MapperName>` mapper Resources folder from Artifactory to the environment.

1. Update the parameters in the `mapper-config.json` file.

1. Seed the mapper configuration to ZooKeeper.

1. Seed the backing application version to ZooKeeper.

{% include custom/series_crosscore_next.html %}
