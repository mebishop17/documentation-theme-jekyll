---
title: Copy Mapper Resources
series: crosscore
weight: 2
sidebar: crosscore_sidebar
permalink: crosscore_copy_mapper.html
folder: crosscore
---

{% include custom/series_crosscore.html %}


In this step, we'll copy the `<MapperName>` Mapper resources folder from Artifactory to the environment. Each CrossCore backing application requires a number of configuration resources. These resources are stored in Artifactory and need to be copied to the Mapper server where the mapper-config-cli tool is installed.
